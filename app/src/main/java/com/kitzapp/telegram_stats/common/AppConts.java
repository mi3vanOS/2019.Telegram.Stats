package com.kitzapp.telegram_stats.common;

/**
 * Created by Ivan Kuzmin on 22.03.2019;
 * 3van@mail.ru;
 * Copyright © 2019 Example. All rights reserved.
 */

public class AppConts {
    public static final String JSON_CHART_FILENAME = "chart_data.json";
    public static final String AUTHOR_LINK = "https://www.linkedin.com/in/mi3van";

    public static final int DELAY_COLOR_ANIM = 220;
    public static final int DELAY_ELEMENTS_ANIM = 220;

    public static final float MAX_CURSORS_WIDTH = 0.20f;

    public static final int INTEGER_MIN_VALUE = 0x80000000;
    public static final int INTEGER_MAX_VALUE = 0x7fffffff;

    public static final int MAX_VALUE_ALPHA = 255;
    public static final int MIN_VALUE_ALPHA = 0;

    public static final int ELEVATION_ACTION_BAR = 6;
    public static final int ELEVATION_CHART_VIEW = 3;
}

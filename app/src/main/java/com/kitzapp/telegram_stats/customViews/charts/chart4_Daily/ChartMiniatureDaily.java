package com.kitzapp.telegram_stats.customViews.charts.chart4_Daily;

import android.annotation.SuppressLint;
import android.content.Context;
import com.kitzapp.telegram_stats.customViews.charts.base.TChartMiniatureView;

/**
 * Created by Ivan Kuzmin on 2019-04-11;
 * 3van@mail.ru;
 * Copyright © 2019 Example. All rights reserved.
 */

@SuppressLint("ViewConstructor")
class ChartMiniatureDaily extends TChartMiniatureView {

    public ChartMiniatureDaily(Context context) {
        super(context);
    }
}

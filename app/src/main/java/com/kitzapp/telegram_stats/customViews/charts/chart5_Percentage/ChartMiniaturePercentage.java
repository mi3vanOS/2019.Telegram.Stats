package com.kitzapp.telegram_stats.customViews.charts.chart5_Percentage;

import android.annotation.SuppressLint;
import android.content.Context;
import com.kitzapp.telegram_stats.customViews.charts.base.TChartMiniatureView;

/**
 * Created by Ivan Kuzmin on 2019-04-11;
 * 3van@mail.ru;
 * Copyright © 2019 Example. All rights reserved.
 */

@SuppressLint("ViewConstructor")
class ChartMiniaturePercentage extends TChartMiniatureView {

    public ChartMiniaturePercentage(Context context) {
        super(context);
    }
}
